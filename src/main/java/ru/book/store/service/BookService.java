package ru.book.store.service;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.book.store.model.Book;
import ru.book.store.repository.BookRepository;
import ru.book.store.repository.CategoryRepository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

@Service
public class BookService {

    private final BookRepository repository;

    public BookService(BookRepository repository, CategoryRepository categoryRepository) {
        this.repository = repository;
    }

    public List<Book> getAllBooks() {
        return repository.findAll();
    }

    public List<Book> getBooks(Integer id, String name, String description, String author, Integer category,
                               Double minPrice, Double maxPrice) {

        Specification<Book> specification = Specification.where(selectById(id)
                .and(selectByName(name))
                .and(selectByDescription(description))
                .and(selectByAuthor(author))
                .and(selectByCategory(category))
                .and(selectByPrice(minPrice, maxPrice))
        );

        return repository.findAll(specification);
    }

    public List<Book> getRandomBooks() {
        return repository.findRandom();
    }

    private Specification<Book> selectById(Integer id) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                if (id == null) {
                    return null;
                }
                return criteriaBuilder.equal(root.get("id"), id);
            }
        };
    }
    private Specification<Book> selectByName(String name) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                if (name == null) {
                    return null;
                }
                return criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
                        "%" + name.toLowerCase() + "%");
            }
        };
    }
    private Specification<Book> selectByDescription(String description) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                if (description == null) {
                    return null;
                }
                return criteriaBuilder.like(criteriaBuilder.lower(root.get("description")),
                        "%" + description.toLowerCase() + "%");
            }
        };
    }

    private Specification<Book> selectByAuthor(String author) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                if (author == null) {
                    return null;
                }
                return criteriaBuilder.like(criteriaBuilder.lower(root.get("author")),
                        "%" + author.toLowerCase() + "%");
            }
        };
    }

    private Specification<Book> selectByCategory(Integer category) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                if (category == null) {
                    return null;
                }
                return criteriaBuilder.or(criteriaBuilder.equal(root.get("category"), category),
                        criteriaBuilder.equal(root.get("category").get("parent"), category));
            }
        };
    }

    public Specification<Book> selectByPrice(Double minPrice, Double maxPrice) {
        return new Specification<Book>() {
            @Override
            public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
                if (minPrice == null && maxPrice == null) {
                    return null;
                }
                if (minPrice == null) {
                    return criteriaBuilder.lessThanOrEqualTo(root.get("price"), maxPrice);
                }
                if (maxPrice == null) {
                    return criteriaBuilder.greaterThanOrEqualTo(root.get("price"), minPrice);
                }
                return criteriaBuilder.between(root.get("price"), minPrice, maxPrice);
            }
        };
    }

}
