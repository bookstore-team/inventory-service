package ru.book.store.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.book.store.model.Book;
import ru.book.store.service.BookService;

import java.util.List;

@RestController
public class BookController {
    private final BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(path = "/allBooks", method = RequestMethod.GET)
    public ResponseEntity getAllBooks() {
        List<Book> books = bookService.getAllBooks();
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @RequestMapping(path = "/books", method = RequestMethod.GET)
    public ResponseEntity getBooks(@RequestParam(name="id", required=false) Integer id,
                                   @RequestParam(name="name", required=false) String name,
                                   @RequestParam(name="description", required=false) String description,
                                   @RequestParam(name="author", required=false) String author,
                                   @RequestParam(name="category", required=false) Integer category,
                                   @RequestParam(name="minPrice", required=false) Double minPrice,
                                   @RequestParam(name="maxPrice", required=false) Double maxPrice) {
        List<Book> books = bookService.getBooks(id, name, description, author, category, minPrice, maxPrice);
        return new ResponseEntity<>(books, HttpStatus.OK);
    }

    @RequestMapping(path = "/randomBooks", method = RequestMethod.GET)
    public ResponseEntity getRandomBooks() {
        List<Book> books = bookService.getRandomBooks();
        return new ResponseEntity<>(books, HttpStatus.OK);
    }
}
