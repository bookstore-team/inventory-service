package ru.book.store;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationRun {
    private static final Logger LOG = LogManager.getLogger(ApplicationRun.class);

    public static void main(String[] args) {
        SpringApplication.run(ApplicationRun.class, args);
        LOG.info("Service started!");
    }
}
