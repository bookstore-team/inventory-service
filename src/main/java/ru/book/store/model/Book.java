package ru.book.store.model;

import javax.persistence.*;

@Entity
@Table(name="book")
public class Book {

    @Id
    @Column(name="id")
    @SequenceGenerator(name = "bookIdSeq", sequenceName = "book_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "bookIdSeq")
    protected Integer id;
    @Column(name="name", length = 50, nullable = false)
    private String name;
    @Column(name="description", length = 200)
    private String description;
    @Column(name="author", length = 50, nullable = false)
    private String author;
    @ManyToOne(fetch = FetchType.LAZY)
    private Category category;
    private Double price;

    public Book() {
    }
    public Book(Integer id, String name, String description, String author, Category category, Double price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.author = author;
        this.category = category;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
