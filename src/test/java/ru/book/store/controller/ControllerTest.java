package ru.book.store.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import ru.book.store.controller.utils.model.BookInfoTest;
import ru.book.store.controller.utils.TestsHelper;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Sql(scripts = "classpath:db/test.sql", config = @SqlConfig(encoding = "UTF-8"))
public class ControllerTest {

    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper mapper = new ObjectMapper();

    @Test
    void getAllBooksTest() throws Exception {
        MvcResult storyResult = mockMvc.perform(get("/allBooks")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);

        assertFalse(storyResult.getResponse().getContentAsString(StandardCharsets.UTF_8).isEmpty());
        assertEquals(10, actual.size());
    }

    @Test
    void getRandomBooksTest() throws Exception {
        MvcResult storyResult = mockMvc.perform(get("/randomBooks")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);

        assertEquals(false, storyResult.getResponse().getContentAsString(StandardCharsets.UTF_8).isEmpty());
        assertEquals(5, actual.size());
    }

    @Test
    void getBooksTest() throws Exception {
        MvcResult storyResult = mockMvc.perform(get("/books")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);
        assertEquals(10, actual.size());
    }

    @Test
    void getBooksByIdTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.NORMAL_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);
        assertEquals(1, actual.size());
        BookInfoTest book = actual.get(0);
        assertEquals(2, book.id);
        assertEquals("Метро. Трилогия под одной обложкой", book.name);
        assertEquals("Дмитрий Глуховский", book.author);
    }

    @Test
    void getBooksByIdStrTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.ID_STR_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void getBooksByNameTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.NAME_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);
        assertEquals(1, actual.size());
        BookInfoTest book = actual.get(0);
        assertEquals(1, book.id);
        assertEquals("Нелл Уайт-Смит", book.author);
        assertEquals(5, book.category.id);
    }

    @Test
    void getBooksByAuthorTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.AUTHOR_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);
        assertEquals(1, actual.size());
        BookInfoTest book = actual.get(0);
        assertEquals(2, book.id);
        assertEquals("Метро. Трилогия под одной обложкой", book.name);
        assertEquals("Дмитрий Глуховский", book.author);
    }

    @Test
    void getBooksByDescriptionTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.DESCRIPTION_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);
        assertEquals(1, actual.size());
        BookInfoTest book = actual.get(0);
        assertEquals(5, book.id);
        assertEquals("Преступление и наказание", book.name);
        assertEquals("Ф.М. Достоевский", book.author);
    }

    @Test
    void getBooksByPriceTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.PRICE_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);
        assertEquals(2, actual.size());
        BookInfoTest book = actual.get(0);
        assertEquals(1, book.id);
        assertEquals(1000D, book.price);
    }

    @Test
    void getBooksByMinPriceTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.MIN_PRICE_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);
        assertEquals(5, actual.size());
        BookInfoTest book = actual.get(0);
        assertEquals(1, book.id);
        assertEquals(1000D, book.price);
    }

    @Test
    void getBooksByMinPriceStrTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.MIN_PRICE_STR_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void getBooksByMaxPriceTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.MAX_PRICE_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);
        assertEquals(3, actual.size());
        BookInfoTest book = actual.get(0);
        assertEquals(4, book.id);
        assertEquals(249.00D, book.price);
    }

    @Test
    void getBooksByMaxPriceStrTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.MAX_PRICE_STR_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    void getBooksByCategoryTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.CATEGORY_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        List<BookInfoTest> actual = getListResult(storyResult);
        assertEquals(3, actual.size());
    }

    @Test
    void getBooksByCategoryStrTest() throws Exception {
        MultiValueMap<String, String> params = getMultiValueMap(TestsHelper.CATEGORY_STR_JSON);

        MvcResult storyResult = mockMvc.perform(get("/books")
                        .params(params)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    private List<BookInfoTest> getListResult(MvcResult storyResult) throws Exception {
        String contentAsString = storyResult.getResponse().getContentAsString(StandardCharsets.UTF_8);

        return mapper.readValue(contentAsString, new TypeReference<>() {
        });
    }

    private MultiValueMap<String, String> getMultiValueMap(String paramStr) throws JsonProcessingException {
        Map<String, String> map = mapper.readValue(paramStr, Map.class);
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        parameters.setAll(map);

        return parameters;
    }
}
