package ru.book.store.controller.utils;

public class TestsHelper {

    public final static String NORMAL_JSON =
            "{" +
                    "\"id\":\"2\"," +
                    "\"author\":\"Дмитрий Глуховский\"," +
                    "\"minPrice\":\"2000\"," +
                    "\"maxPrice\":\"2400\"" +
                    "}";
    public final static String ID_STR_JSON =
            "{" +
                    "\"id\":\"STR\"" +
                    "}";
    public final static String NAME_JSON =
            "{" +
                    "\"name\":\"Демонология\"" +
                    "}";
    public final static String AUTHOR_JSON =
            "{" +
                    "\"author\":\"Дмитрий\"" +
                    "}";
    public final static String DESCRIPTION_JSON =
            "{" +
                    "\"description\":\"Наказание\"" +
                    "}";
    public final static String PRICE_JSON =
            "{" +
                    "\"minPrice\":\"500\"," +
                    "\"maxPrice\":\"1500\"" +
                    "}";
    public final static String MIN_PRICE_JSON =
            "{" +
                    "\"minPrice\":\"500\"" +
                    "}";
    public final static String MIN_PRICE_STR_JSON =
            "{" +
                    "\"minPrice\":\"STR\"" +
                    "}";
    public final static String MAX_PRICE_JSON =
            "{" +
                    "\"maxPrice\":\"400\"" +
                    "}";
    public final static String MAX_PRICE_STR_JSON =
            "{" +
                    "\"minPrice\":\"STR\"" +
                    "}";
    public final static String CATEGORY_JSON =
            "{" +
                    "\"category\":\"1\"" +
                    "}";
    public final static String CATEGORY_STR_JSON =
            "{" +
                    "\"category\":\"STR\"" +
                    "}";
}
