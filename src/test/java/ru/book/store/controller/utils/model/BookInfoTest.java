package ru.book.store.controller.utils.model;

import java.util.Objects;

public class BookInfoTest {

    public Integer id;
    public String name;
    public String description;
    public String author;
    public CategoryInfoTest category;
    public Double price;

    public BookInfoTest() {
    }

    public BookInfoTest(Integer id, String name, String description, String author, CategoryInfoTest category, Double price) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.author = author;
        this.category = category;
        this.price = price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, author, category, price);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookInfoTest that = (BookInfoTest) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(author, that.author) &&
                category == that.category &&
                Objects.equals(price, price);
    }
}
