package ru.book.store.controller.utils.model;

import java.util.Objects;

public class CategoryInfoTest {

    public Integer id;
    public String name;
    public CategoryInfoTest parent;

    public CategoryInfoTest() {
    }

    public CategoryInfoTest(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public CategoryInfoTest(Integer id, String name, CategoryInfoTest parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, parent);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryInfoTest that = (CategoryInfoTest) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(parent, (that.parent));
    }
}
