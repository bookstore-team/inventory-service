package ru.book.store.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlConfig;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
@Sql(scripts = "classpath:db/test.sql", config = @SqlConfig(encoding = "UTF-8"))
public class InMemoryDBTest {

    @Autowired
    private BookService bookService;

    @Test
    public void getAllBooksTest() {
        assertEquals(true, bookService.getAllBooks().size() > 0);
    }

    @Test
    public void getRandomBooks() {
        assertEquals(true, bookService.getRandomBooks().size() <= 5);
    }
}

