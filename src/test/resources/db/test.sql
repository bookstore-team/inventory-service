drop table if exists order_book;
drop table if exists order_pay;
drop table if exists book;
drop table if exists category;
drop table if exists "order";
drop table if exists customer;

drop sequence if exists category_id_seq;
drop sequence if exists book_id_seq;

create table if not exists customer (
    id serial PRIMARY KEY,
    username VARCHAR ( 50 ) UNIQUE NOT NULL,
    password VARCHAR ( 50 ) NOT NULL,
    email VARCHAR ( 255 ) UNIQUE NOT NULL,
    created_on TIMESTAMP NOT NULL,
    last_login TIMESTAMP,
    first_name VARCHAR ( 50 ),
    last_name VARCHAR ( 50 ),
    address VARCHAR ( 50 )
);

create table if not exists "order" (
    id serial PRIMARY KEY,
    customer_id INT NOT NULL,
    timedate TIMESTAMP NOT NULL,
    price DECIMAL NOT null,
    FOREIGN KEY (customer_id)
        REFERENCES customer (id)
);

create table if not exists category (
    id serial PRIMARY KEY,
    category_name VARCHAR ( 50 ) UNIQUE NOT NULL,
    category_parent_id INT,
    FOREIGN KEY (category_parent_id)
        REFERENCES category (id)
);

create sequence if not exists category_id_seq START WITH 1 INCREMENT BY 1;

create table if not exists book (
    id serial PRIMARY KEY,
    name VARCHAR ( 50 ) NOT NULL,
    description VARCHAR ( 200 ) NOT NULL,
    author VARCHAR ( 50 ) NOT NULL,
    price INT,
    category_id INT,
    FOREIGN KEY (category_id)
        REFERENCES category (id)
);

create sequence if not exists book_id_seq START WITH 1 INCREMENT BY 1;

create table if not exists order_book (
    id serial PRIMARY KEY,
    order_id INT NOT NULL,
    book_id INT NOT null,
    FOREIGN KEY (order_id)
        REFERENCES "order" (id),
    FOREIGN KEY (book_id)
        REFERENCES book (id)
);

create table if not exists order_pay (
    id serial PRIMARY KEY,
    summ DECIMAL NOT NULL,
    pay_datetime TIMESTAMP, --добавится если paid = true
    customer_id INT,
    order_id INT,
    paid BOOLEAN NOT NULL,
    customer_num_card INT,
    customer_bank VARCHAR ( 50 ),
    FOREIGN KEY (customer_id)
       REFERENCES customer (id),
    FOREIGN KEY (order_id)
       REFERENCES "order" (id)
);

insert into category (id, category_name, category_parent_id)
    values (nextval('category_id_seq'), 'Фантастика', null),
           (nextval('category_id_seq'), 'Художественная литература', null),
           (nextval('category_id_seq'), 'Туризм', null),
           (nextval('category_id_seq'), 'Романтика', null);
insert into category (id, category_name, category_parent_id)
    values (nextval('category_id_seq'), 'Киберпанк', (select id from category where category_name = 'Фантастика')),
           (nextval('category_id_seq'), 'Стимпанк', (select id from category where category_name = 'Фантастика')),
           (nextval('category_id_seq'), 'Фэнтези', (select id from category where category_name = 'Фантастика'));

insert into book (id, name, description, author, price, category_id)
    values (nextval('book_id_seq'), 'Демонология и я. Сны Зимы', 'Вы прочтёте историю чёрного механического кота-оборотня, который интересуется больше всего на свете демонами.',
                'Нелл Уайт-Смит', 1000, (select id from category where category_name = 'Киберпанк')),
           (nextval('book_id_seq'), 'Метро. Трилогия под одной обложкой', 'Вы прочтёте историю чёрного механического кота-оборотня, который интересуется больше всего на свете демонами.',
                'Дмитрий Глуховский', 2395, (select id from category where category_name = 'Фэнтези')),
           (nextval('book_id_seq'), 'Война и мир', 'Величайшая эпопея, снискавшая славу во всем мире. Сотни героев, застигнутых безжалостным потоком времени, и сотни судеб, перемолотых масштабными историческими событиями.',
                'Лев Толстой', 1145, (select id from category where category_name = 'Художественная литература')),
           (nextval('book_id_seq'), 'Мастер и Маргарита', 'Бессмертное, загадочное и остроумное «Евангелие от Сатаны» Михаила Булгакова.',
                'М.А. Булгаков', 249, (select id from category where category_name = 'Художественная литература')),
           (nextval('book_id_seq'), 'Преступление и наказание', '«Преступление и наказание» (1866) — одно из самых значительных произведений в истории мировой литературы.',
                'Ф.М. Достоевский', 279, (select id from category where category_name = 'Художественная литература')),
           (nextval('book_id_seq'), 'Ведьмак', 'Одна из лучших фэнтези-саг за всю историю существования жанра.',
                'Анджей Сапковский', 1637, (select id from category where category_name = 'Фэнтези')),
           (nextval('book_id_seq'), 'Собакистан', 'Закрытая социалистическая республика Собакистан открывает границы главам дружественных государств и избранным журналистам в честь репетиции похорон лидера республики - товарища Дружка.',
                'Виталий Терлецкий', 492, (select id from category where category_name = 'Художественная литература')),
           (nextval('book_id_seq'), 'Мир, я люблю тебя!', 'А вы когда-нибудь задумывались, зачем вообще люди путешествуют?',
                'Жданов А.', 1690, (select id from category where category_name = 'Туризм')),
           (nextval('book_id_seq'), 'Лето в пионерском галстуке', 'Юра возвращается в пионерский лагерь своей юности спустя двадцать лет.',
                'Сильванова Катерина, Малисова Елена', 299, (select id from category where category_name = 'Туризм')),
           (nextval('book_id_seq'), 'Нелюбовь сероглазого короля', 'Даша Севастьянова думала, что единственное, о чем ей стоит беспокоиться в последний учебный год,— это предстоящие экзамены, но внезапно проблемы стали нарастать как снежный ком.',
                'Ася Лавринович', 494, (select id from category where category_name = 'Туризм'));